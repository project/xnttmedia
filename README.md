External Entities Media Files plugin
************************************

Enables to manage media files as external entities.

===============================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

.

REQUIREMENTS
------------

This module requires the following modules:

 * [External Entities](https://www.drupal.org/project/external_entities)

 * [getID3](https://github.com/JamesHeinrich/getID3)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.
 * If you enabled the module using Drush, you must also rebuild the cache.

CONFIGURATION
-------------

The module has no menu or modifiable global settings. There is no configuration.
When enabled, the module will add a new storage client for external entity
types. Then, when you create a new external entity type, you can select the
"Media files" plugin and have access to settings specific to the new external
entity.

MAINTAINERS
-----------

Current maintainers:
 * Valentin Guignon (guignonv) - https://www.drupal.org/u/guignonv
