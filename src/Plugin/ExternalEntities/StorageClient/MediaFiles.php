<?php

namespace Drupal\xnttmedia\Plugin\ExternalEntities\StorageClient;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\external_entities\Plugin\ExternalEntities\StorageClient\Files;

/**
 * External entities storage client for media files.
 *
 * @StorageClient(
 *   id = "xnttmedia",
 *   label = @Translation("Media files with ID3 data"),
 *   description = @Translation("Retrieves ID3 data from media files.")
 * )
 */
class MediaFiles extends Files {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    TranslationInterface $string_translation,
    LoggerChannelFactoryInterface $logger_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    ConfigFactory $config_factory,
    MessengerInterface $messenger,
    CacheBackendInterface $cache
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $string_translation,
      $logger_factory,
      $entity_type_manager,
      $entity_field_manager,
      $config_factory,
      $messenger,
      $cache
    );
    // Defaults.
    $this->fileType = 'media';
    $this->fileTypePlural = 'medias';
    $this->fileTypeCap = 'Media';
    $this->fileTypeCapPlural = 'Media';
    // Default to AVI but it could be many others.
    $this->fileExtensions = [
      '.avi',
      '.3ga',
      '.aa',
      '.aac',
      '.ac3',
      '.aif',
      '.aifc',
      '.aiff',
      '.amr',
      '.amv',
      '.ape',
      '.asf',
      '.au',
      '.bmp',
      '.cda',
      '.cue',
      '.div',
      '.divx',
      '.dvx',
      '.dsd',
      '.dsf',
      '.dss',
      '.flac',
      '.gif',
      '.gz',
      '.iso',
      '.j2c',
      '.j2k',
      '.jp2',
      '.jpc',
      '.jpeg',
      '.jpf',
      '.jpg',
      '.jpg2',
      '.jpm',
      '.jpx',
      '.mid',
      '.midi',
      '.mj2',
      '.mkv',
      '.mov',
      '.movie',
      '.mp1',
      '.mp2',
      '.mp3',
      '.mp4',
      '.mpc',
      '.mpeg',
      '.mpg',
      '.nsv',
      '.ogg',
      '.pdf',
      '.png',
      '.qt',
      '.rmvb',
      '.rv',
      '.swf',
      '.tar',
      '.tgz',
      '.tiff',
      '.voc',
      '.vqf',
      '.wav',
      '.webp',
      '.wma',
      '.wmv',
      '.wv',
      '.wvc',
      '.zip',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function parseFile(string $file_path) :array {
    $data = parent::parseFile($file_path);
    $id = array_keys($data)[0] ?? NULL;
    // Add media metadata infos.
    if (class_exists('getID3')) {
      $getID3 = new \getID3();
      $media_data = $getID3->analyze($file_path);
      $getID3->CopyTagsToComments($media_data);
      if (isset($id)) {
        $data[$id] += $media_data;
      }
    }
    else {
      $this->messenger->addWarning(
        $this->t(
          'Unable to load media metadata: getID3 library is not installed or enabled.'
        )
      );
      $this->logger->warning(
        'Unable to load metadata from file "'
        . $file_path
        . '": PHP getID3 library is not installed or enabled.'
      );
    }
    return $data;
  }

}
